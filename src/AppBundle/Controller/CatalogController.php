<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Author;
use AppBundle\Entity\Book;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CatalogController extends Controller
{
    public function getAll() {
        $em = $this->getDoctrine()->getManager();
        try {
            $connection = $em->getConnection();
            $statement = $connection->prepare("SELECT * FROM author JOIN (SELECT * FROM book JOIN author_book 
            ON book.ID = author_book.book_id) AS ab ON author.ID = ab.author_id");
            $statement->execute();
            $results = $statement->fetchAll();
            for ($i = 0; count($results) > $i; $i++) {
                $results[$i]["FIO"] = preg_replace('#(.*)\s+(.).*\s+(.).*#usi', '$1 $2.$3.', $results[$i]["FIO"]);
            }
            return $results;
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function getBookByAuthorId(int $id): array
    {
        $em = $this->getDoctrine()->getManager();
        try {
            $connection = $em->getConnection();
            $statement = $connection->prepare("SELECT author.*, book.book_name FROM author 
            LEFT OUTER JOIN author_book ON author.ID = author_book.author_id AND author_book.book_id = :id 
            LEFT OUTER JOIN book ON author_book.book_id = book.ID");
            $statement->bindValue('id', $id);
            $statement->execute();
            $results = $statement->fetchAll();
            $result = [];
            for ($i = 0; $i < count($results); $i++) {
                if ($results[$i]["book_name"]) {
                    $result[] = [
                        'FIO' => $results[$i]["FIO"],
                        'book_name' => $results[$i]["book_name"],
                    ];
                }
            }
            return $result;
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    /**
     * @Route("/", name="view_index")
     */
    public
    function viewIndex()
    {
        $allData = $this->getAll();
        return $this->render('pages/index.html.twig', array(
            'allData' => $allData,
        ));

    }

    /**
     * @Route("/post/create", name="create_post_route")
     */
    public
    function createPostAction()
    {
        $author = $this->getDoctrine()->getRepository('AppBundle:Authors')->findAll();
        print_r($author);
        exit();
        return $this->render("pages/create.html.twig");
    }

}
