<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('book_name', TextType::class, array('label' => 'Название книги', 'attr' => array('class' => 'form-control')))
            ->add('publish_year', TextType::class, array('label' => 'Год публикации', 'attr' => array('class' => 'form-control')))
            ->add('ISBN', TextType::class, array('label' => 'ISBN', 'attr' => array('class' => 'form-control')))
            ->add('page_size', TextType::class, array('label' => 'Количество страниц', 'attr' => array('class' => 'form-control')))
            ->add('cover', FileType::class, array('required' => false, 'label' => 'Добавить обложку'))
            ->add('save', SubmitType::class, array('label' => 'Добавить книгу'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Book'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_book';
    }


}
